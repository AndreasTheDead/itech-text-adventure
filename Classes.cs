﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using TextAdventure;

namespace Classes
{
    //The Class Language describes the Method's and Structure of the Object in which every String which can be Outputted is saved.
    public class Language
    {
        public Dictionary<string, List<string>> Content { get; set; }
        //Adds a Folder/File Combination to the Dict.
        public void Add(string key, List<string> value)
        {
            this.Content.Add(key, value);
        }
        //Gets a String from the Dict
        public string Get(string teil, int index)
        {
            return this.Content[teil][index];
        }
        //Gets a all Strings to be used in one Place.Talk from the Dict
        public List<string> GetGeneralTalk(string Place)
        {
            return this.Content["Characters_"+Place];
        }
        //Gets the TalkTo strings from the dict (For the structure of them look at https://gitlab.com/AndreasTheDead/itech-text-adventure#3-usage-of-talkto-reden-mit)
        //and Returns it as a new Dict)
        public Dictionary<string, List<string>> GetConversation(string Character, string room)
        {
            Dictionary<string, List<string>> ReturnData = new Dictionary<string, List<string>>();
            ReturnData.Add("Character", this.Content["Characters_"+Character+"_"+room]);
            ReturnData.Add("Protagonist", this.Content["Characters_" + Character + "_" + room+ "_answers"]);
            return ReturnData;
        }
    }
    //The Class Text Manages the Writing of Text and the Public Storage of The Language Dict.
    public class Text
    {
        public static Language Output = new Language();
        //Imports Dynamically every .json file from the sub-folders in (Only on level down) : Language\[Language code]\
        //The Language Code is set in The 'TextAdventure.exe.config' file
        //Then Saves the Content under the Folder_Filename key in the Output Dict.
        public static void Import()
        {
            Text.Output.Content = new Dictionary<string, List<string>>();
            string LanguageCode = ConfigurationManager.AppSettings.Get("GameLanguage");
            string LanguageRootFolder = AppDomain.CurrentDomain.BaseDirectory + "Language\\" + LanguageCode + "\\";
            string[] subdirectoryEntries = Directory.GetDirectories(LanguageRootFolder);
            foreach (string folder in subdirectoryEntries)
            {
                string[] files = Directory.GetFiles(folder);
                foreach (string file in files)
                {
                    string key_name = file.Replace(LanguageRootFolder, "").Replace("\\", "_").Replace(".json", "");
                    Text.Output.Add(key_name, ReadJSON(file));
                }
            }
        }
        //Reads the JSON files and converts them to a List, Returns the List then to the Text.Import() function
        static List<string> ReadJSON(string filepath)
        {
            //Information from https://stackoverflow.com/questions/39539395/deserialize-json-net-json-file-directly-to-a-c-sharp-dictionary-type
            dynamic jsonfile = JsonConvert.DeserializeObject(File.ReadAllText(filepath));
            List<string> liste = new List<string>();
            foreach (string entry in jsonfile)
            {
                liste.Add(entry);
            }
            return liste;
        }
        //Writes a String one Char at the time for a better feeling and ads a new line at the end.
        public static void Write(string text) {
            //somethings from https://stackoverflow.com/questions/10541124/wrap-text-to-the-next-line-when-it-exceeds-a-certain-length
            int LineLimit = 100;
            string[] words = text.Split(' ');
            StringBuilder newSentence = new StringBuilder();
            string CurrentLine = "";
            foreach (string word in words)
            {
                if ((CurrentLine + word).Length > LineLimit)
                {
                    newSentence.AppendLine(CurrentLine);
                    CurrentLine = "";
                }
                CurrentLine += string.Format("{0} ", word);
            }
            if (CurrentLine.Length > 0)
                newSentence.AppendLine(CurrentLine);
            foreach (char c in newSentence.ToString())
            {
                Console.Write(c);
                Thread.Sleep(45);
            }
        }
        //Writes a String one Char at the time for a better feeling and ads a no new line at the end.
        public static void WriteNoNewLine(string text)
        {
            foreach (char c in text)
            {
                Console.Write(c);
                Thread.Sleep(50);
            }
        }
        //Takes a List and writes the list with Write()
        public static void WriteList(List<string> List)
        {
            foreach (string entry in List)
            {
                Write(entry);
            }
        }
        //Clears the last X lines from the Console.
        public static void ClearLastLines(int lines)
        {
            int line = 1;
            while (line <= lines)
            {
                //Inspired by https://stackoverflow.com/questions/8946808/can-console-clear-be-used-to-only-clear-a-line-instead-of-whole-console
                Console.SetCursorPosition(0, Console.CursorTop - 1);
                Console.Write(new string(' ', Console.WindowWidth));
                Console.SetCursorPosition(0, Console.CursorTop - 1);
                line++;
            }
        }
    }
    //A Class which is inherited to the Characters 
    public class AnimalSpecies
    {
        string SpeziesName { get; set; }
        string SpeziesSize { get; set; }
        string SpeziesColor { get; set; }
        string SpeziesSkin { get; set; }

        //An easy way to fill the Object with data in an One-liner
        public void FillSpezie(string name, string size, string color, string skin)
        {
            this.SpeziesName = name;
            this.SpeziesSize = size;
            this.SpeziesColor = color;
            this.SpeziesSkin = skin;
        }

    }

    //The Class were every Location of the CGame is Created in and filled with method's to interact with the Location
    public class Places
    {
        public string Name { get; set; }
        string Description { get; set; }
        public bool CanTalk = true;
        public bool CanTalkTo = false;
        public bool CanWalk = false;
        public bool CanQuestlist = true;
        public bool ClearPreviousText { get; set; }
        public bool IstEndRoom { get; set; }
        public List<Characters> People { get; set; }
        public List<Places> NextPlaces { get; set; }
        public List<Places> PreviousPlace { get; set; }
        //a Constructor to Create the Object with the Right Values.
        public void Fill(string Name)
        {
            this.Name = Name;
            //Gets the Description from a .json File for Localization. https://gitlab.com/AndreasTheDead/itech-text-adventure#3-file-creation
            this.Description = Text.Output.Get("Places_" + this.Name.Replace(" ", "_"), 0);
            //Creates Empty Lists for People, NextPlaces and Previous Places
            this.People = new List<Characters>();
            this.NextPlaces = new List<Places>();
            this.PreviousPlace = new List<Places>();
            this.ClearPreviousText = false;
            this.IstEndRoom = false;
        }
        //Writes the Description of the Place in the Console
        void GetDescription()
        {
            Text.Write(this.Description);
        }
        //Method which is run every time Someone Enters the Place
        //Writes the description, Sets the Current Place of the MainPlayer, and Opens the Choice Method
        public void Enter()
        {
            if (this.ClearPreviousText == true)
            {
                Console.Clear();
            }
            this.GetDescription();
            Program.Protagonist.CurrentPlace = this;
            this.Choice();
        }
        //Method if the Room has an interaction you can start it with Talk (Reden)
        private void Talk()
        {
            //Writes every entry of the .json of the Character_Place
            foreach (string line in Text.Output.GetGeneralTalk(this.Name.Replace(" ", "_")))
            {
                Text.Write(line);
            }
            //If there is no CanTalkTo gives the Player the quests he didn't already finished or already has and Completes the Quests which are done.
            if (this.CanTalkTo == false)
            {
                foreach (Characters person in this.People)
                {
                    Text.Write("");
                    foreach (Quests Quest in person.QuestGiver)
                    {
                        if ((Quest.Finished == false)&&(!Program.Protagonist.ActiveQuests.Contains(Quest)))
                        {
                            Quest.AddQuest();
                        }
                    }
                    foreach (Quests Quest in person.QuestReceiver)
                    {
                        Quest.RemoveQuest();
                        Quest.Finished = true;
                    }
                }
            }
        }
        //Method which asks the Player what he wants to do:
        public void Choice()
        {
            //Checks if it is the Last Place if yes The game will play the finish Scene and Exits.
            if (this.IstEndRoom == true)
            {
                Text.Write(Text.Output.Get("Other_Ending",0));
                Text.Write(Text.Output.Get("Other_Ending",1));
                Text.Write(Text.Output.Get("Other_Generell", 15));
                string placeholder = Console.ReadLine();
                Environment.Exit(0);
            }
            //If it is Not the Last Place.
            //Asks what the Player whats to do and prints the available Options (defines with Place.CanWalk, Place.CanTalkTo, Place.CanTalk, Place.CanQuestlist
            List<string> Choices = Text.Output.Get("Other_Generell", 0).Split(';').ToList();
            Text.Write(Choices[0]);
            string options = "";
            if (this.CanWalk == true) { options += Choices[1] + ", "; }
            if (this.CanTalkTo == true) { options += Choices[2] + ", "; }
            if (this.CanTalk == true) { options += Choices[3] + ", "; }
            if (this.CanQuestlist == true) { options += Choices[4] + ", "; }
            Text.Write("["+options.Substring(0,options.Length-2)+"]");
            //Creates a Array with every Possible Number and an Array with every possible Option
            string[] posibilities = new string[4];
            if (this.CanWalk == true) { posibilities[0] = Choices[1]; } //Weitergehen
            if (this.CanTalkTo == true) { posibilities[1] = Choices[2]; }//Reden mit
            if (this.CanTalk == true) { posibilities[2] = Choices[3]; }//Reden
            if (this.CanQuestlist == true) { posibilities[3] = Choices[4]; } //Questlist
            string[] numbers = new string[10];
            numbers[0] = "0";
            numbers[1] = "1";
            numbers[2] = "2";
            numbers[3] = "3";
            numbers[4] = "4";
            numbers[5] = "5";
            numbers[6] = "6";
            numbers[7] = "7";
            numbers[8] = "8";
            numbers[9] = "9";
            //Reads the Choice and checks if the Choice is valid
            string Choiceinput = Console.ReadLine();
            while (!posibilities.Contains(Choiceinput))
            {
                Text.Write(Text.Output.Get("Other_Generell", 4));
                Choiceinput = Console.ReadLine();
            }
            Text.ClearLastLines(3);
            //if "Weitergehen" is the Choice this code will run
            if (Choiceinput == Choices[1])
            {
                Text.Write(Text.Output.Get("Places_" + this.Name.Replace(" ", "_"), 1));
                Text.Write("");
                Text.Write(Text.Output.Get("Other_Generell", 1));
                //writes out the Available Options to Walk to.
                if (this.NextPlaces.Count == 1)
                {
                    Text.Write("0: " + this.NextPlaces[0].Name);
                }
                else
                {
                    int indexnr = 0;
                    foreach (Places Place in this.NextPlaces)
                    {
                        Text.Write(indexnr.ToString() + ": " + Place.Name);
                        indexnr++;
                    }
                }
                string nextplaceinput = Console.ReadLine();
                //Checks if the Input Number is a Valid answer if not asks for an valid answer
                while ((!numbers.Contains(nextplaceinput)) || (int.Parse(nextplaceinput) >= this.NextPlaces.Count))
                {
                    Text.Write(Text.Output.Get("Other_Generell", 3));
                    nextplaceinput = Console.ReadLine();
                    Text.ClearLastLines(2);
                }
                Text.ClearLastLines(this.NextPlaces.Count + 3);
                //Changes the Current Location to the New Selected Location
                this.NextPlaces[int.Parse(nextplaceinput)].Enter();
            }
            //If "Reden To" is the Choice this code will run
            else if (Choiceinput == Choices[2])
            {
                //Checks if there are Persons in the Place and if yes Asks for the Person who you want to talk to and checks if the section is valid
                if (this.People.Count != 0)
                {
                    Text.Write(Text.Output.Get("Other_Generell", 2));
                    if (this.People.Count == 1)
                    {
                        Text.Write("0: " + this.People[0].Name);
                    }
                    else
                    {
                        int indexnr = 0;
                        //Writes Every Person with here key in Places.People
                        foreach (Characters character in this.People)
                        {
                            Text.Write(indexnr.ToString() + ": " + character.Name);
                        }
                    }
                    string characterinput = Console.ReadLine();
                    while ((!numbers.Contains(characterinput)) || (int.Parse(characterinput) >= this.People.Count))
                    {
                        Text.Write(Text.Output.Get("Other_Generell", 3));
                        characterinput = Console.ReadLine();
                        Text.ClearLastLines(2);
                    }
                    Text.ClearLastLines(this.People.Count + 2);
                    //Runs the Talk to Method from the Class Places
                    this.People[int.Parse(characterinput)].TalkTo();
                }
                else
                {
                    //Writes an error if There is no Person who can be talked to
                    Text.Write(Text.Output.Get("Other_Generell", 7));
                    this.Choice();
                }
            }
            //If "Reden" is the Choice this code will run
            else if (Choiceinput == Choices[3])
            {
                //runs the Talk method, sets CAntalk to false to Prevent multiple runs of it and then asks for the Next Choice
                this.Talk();
                this.CanTalk = false;
                this.CanWalk = true;
                this.Choice();
            }
            //if "Questlist" is choice this code will run
            else if (Choiceinput == Choices[4])
            {
                //Writes the Name of every Quest and Asks if the Player wants to get the Description of one Quest.
                //Has the Option to select 0 if the Player doesn't want any more info.
                Text.Write(Text.Output.Get("Other_Generell", 8));
                if (Program.Protagonist.ActiveQuests.Count >= 1)
                {
                    int QIndex = 1;
                    Text.Write(Text.Output.Get("Other_Generell", 10));
                    //Writes all Active Quest names with there Keyvalue+1 from Protagonist.ActiveQuests
                    foreach (Quests Quest in Program.Protagonist.ActiveQuests)
                    {
                        Text.Write(QIndex + ": " + Quest.Name);
                        QIndex++;
                    }
                    Text.Write("");
                    Text.Write(Text.Output.Get("Other_Generell", 9));
                    string questinput = Console.ReadLine();
                    //Checks if the Input is a Valid Value
                    while ((int.Parse(questinput) >= QIndex) || (!numbers.Contains(questinput)))
                    {
                        Text.Write(Text.Output.Get("Other_Generell", 4));
                        questinput = Console.ReadLine();
                        Text.ClearLastLines(2);
                    }
                    Text.ClearLastLines(5 + Program.Protagonist.ActiveQuests.Count);
                    //if the player don't want any more information exists to the Choice window.
                    if (int.Parse(questinput) == 0)
                    {
                        this.Choice();
                    }
                    else
                    //If a Quest is Selected for More Information Writes those informations
                    {
                        Program.Protagonist.ActiveQuests[int.Parse(questinput) - 1].GetLog();
                        this.Choice();
                    }
                }
                //Writes an error if there are No ActiveQuests
                else if (Program.Protagonist.ActiveQuests.Count == 0)
                {
                    Text.Write(Text.Output.Get("Other_Generell", 12));
                    this.Choice();
                }
            }
        }
    }
    //This class Creates every Character
    public class Characters : AnimalSpecies
    {
        public string Name { get; set; }
        bool IsNpc { get; set; }
        string Gender { get; set; }
        string Haircolor { get; set; }
        bool talked { get; set; }
        public List<Quests> ActiveQuests { get; set; }
        public List<Quests> QuestGiver { get; set; }
        public List<Quests> QuestReceiver { get; set; }
        public Places CurrentPlace { get; set; }
        //Adds an Easy way to Fill the Object with data (Like a Constructor)
        public Characters(string Name, bool IsNpc, string Gender, string Haircolor, Places CurrentPlace, bool talked = false)
        {
            this.Name = Name;
            this.IsNpc = IsNpc;
            this.Gender = Gender;
            this.Haircolor = Haircolor;
            this.talked = talked;
            this.CurrentPlace = CurrentPlace;
            this.ActiveQuests = new List<Quests>();
            this.QuestGiver = new List<Quests>();
            this.QuestReceiver = new List<Quests>();
        }
        //This Method adds the Possibility to have a Talk with answers and answers Possibility with a Character 
        public void TalkTo()
        {
            //Gets the Whole Conversation from the Language Dict.
            //Conversations are build like: https://gitlab.com/AndreasTheDead/itech-text-adventure#3-usage-of-talkto-reden-mit
            Dictionary<string, List<string>> conversation = Text.Output.GetConversation(this.Name, this.CurrentPlace.Name.Replace(" ","_"));
            int sections = conversation["Character"].Count;
            int x = 0;
            string LastOptionSelected = "0";
            Text.Write(Text.Output.Get("Other_Generell", 6).Replace("%person%", this.Name));
            if (this.talked == false)
            {
                while (x < sections - 1)
                {
                    //Character which is talked to
                    //If in one string is a ; then there can be different this written out.
                    //The ; needs to be first at a Player Answer Possibility where the string is split at the ; and each part of the string is one Possibility which can be selected.
                    //After this the Selection key is saved in an var and can be used in a Sentence of the Character.
                    if (conversation["Character"][x].Contains(';'))
                    {
                        List<string> options = conversation["Character"][x].Split(';').ToList<string>();
                        Text.Write(this.Name + ": " + options[int.Parse(LastOptionSelected)]);
                    }
                    else
                    {
                        Text.Write(this.Name + ": " + conversation["Character"][x]);
                    }
                    //Answers of the Player
                    if (conversation["Protagonist"][x].Contains(';'))
                    {
                        List<string> options = conversation["Protagonist"][x].Split(';').ToList<string>();
                        int index = 0;
                        Text.Write(Text.Output.Get("Other_Generell", 5));
                        Text.Write(" ");
                        foreach (string option in options)
                        {
                            Text.Write(index.ToString() + ": " + options[index]);
                            index++;
                        }
                        LastOptionSelected = Console.ReadLine();
                        //creates ab Array with every number as char and checks if the input entered is valid.
                        string[] numbers = new string[10];
                        numbers[0] = "0";
                        numbers[1] = "1";
                        numbers[2] = "2";
                        numbers[3] = "3";
                        numbers[4] = "4";
                        numbers[5] = "5";
                        numbers[6] = "6";
                        numbers[7] = "7";
                        numbers[8] = "8";
                        numbers[9] = "9";
                        while ((!numbers.Contains(LastOptionSelected)) || (int.Parse(LastOptionSelected) >= options.Count))
                        {
                            Text.Write(Text.Output.Get("Other_Generell", 4));
                            LastOptionSelected = Console.ReadLine();
                            Text.ClearLastLines(2);
                        }
                        //Deletes the Choices and only writes the selected choice
                        Text.ClearLastLines(options.Count + 3);
                        Text.Write("Protagonist: " + options[int.Parse(LastOptionSelected)]);
                    }
                    //If there is not an ; writes the whole string
                    else
                    {
                        Text.Write("Protagonist:" + conversation["Protagonist"][x]);
                    }
                    x++;
                }
                //sets the Characters to talked so the character can answer something else the next time
                this.talked = true;
                //Gives the Player the Quests the Character has and takes the Quest the Player has finished
                if ((this.QuestGiver.Count != 0) || (this.QuestReceiver.Count != 0))
                {
                    Text.Write("");
                    foreach (Quests Quest in this.QuestGiver)
                    {
                        if ((Quest.Finished == false) && (Program.Protagonist.ActiveQuests.Contains(Quest)))
                        {
                            Quest.AddQuest();
                        }
                    }
                    foreach (Quests Quest in this.QuestReceiver)
                    {
                        Quest.RemoveQuest();
                        Quest.Finished = true;
                    }
                }
            }
            //If the Player already had talked with this Character gives an Special Output
            else if (this.talked == true)
            {
                Text.Write(conversation["Character"][sections-1]);
            }
            //Restarts the Choice window for the Player
            Program.Protagonist.CurrentPlace.Choice();
        }
    }

    //he Class Defines every Quest the Player can have.
    public class Quests
    {
        public string Name { get; set; }
        string Description { get; set; }
        public bool Finished { get; set; }
        Characters Employer { get; set; }
        Characters Costumer { get; set; }
        //Adds a MEthode to Fill the Object with Information with an One liner
        public void Fill(string name, Characters employer, Characters costumer)
        {
            Name = name;
            Employer = employer;
            Costumer = costumer;
            Finished = false;
            Description = Text.Output.Get("Quests_" + this.Name.Replace(' ', '_'), 0);
        }
        //Writes the Detailed Information of a Quest
        public void GetLog()
        {
            List<string> DescriptionItems = Text.Output.Get("Other_Generell", 11).Split(';').ToList<string>();
            Text.Write(DescriptionItems[0] + Name);
            Text.Write(DescriptionItems[1] + Employer.Name);
            Text.Write(DescriptionItems[2] + Description);
            Text.Write(DescriptionItems[3] + Costumer.Name);
        }
        //Adds the Quest to the ActiveQuests of a Player
        public void AddQuest()
        {
            Program.Protagonist.ActiveQuests.Add(this);
            Text.Write(Text.Output.Get("Other_Generell", 13).Replace("%quest%", this.Name));
        }
        //Removes the Quest to the ActiveQuests of a Player
        public void RemoveQuest()
        {
            Program.Protagonist.ActiveQuests.Remove(this);
            Text.Write(Text.Output.Get("Other_Generell", 14).Replace("%quest%", this.Name));
        }

    }

}