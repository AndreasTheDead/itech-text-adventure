﻿using System.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Newtonsoft.Json;
using System.Collections.Specialized;
using System.Media;
using Classes;
using System.Xml.Schema;
using System.Security.Cryptography.X509Certificates;

namespace TextAdventure
{
    public class Program
    {
        //Creation of Objects (Places)
        public static Places Fuchsbau = new Places();
        public static Places Weg_Versamlungsplatz = new Places();
        public static Places Weg_großes_Tor = new Places();
        public static Places Versammlungsplatz = new Places();
        public static Places Großes_Tor = new Places();
        public static Places Schneise_durch_Wald = new Places();
        public static Places Fluss_Ufer_Ost = new Places();
        public static Places Weg_durch_Wald = new Places();
        public static Places Fluss_Ufer_West = new Places();
        public static Places Eulen_Hügel = new Places();
        public static Places Zuhause_der_Eule = new Places();
        public static Places Fluss_Ufer_Süden = new Places();
        public static Places Links_Wald = new Places();
        public static Places Mitte_Wald = new Places();
        public static Places Rechts_Wald = new Places();
        public static Places Kristall_Höhle = new Places();
        public static Places Schlucht = new Places();
        public static Places Kristall_Wald = new Places();
        public static Places Tal = new Places();
        public static Places Baumhöhle = new Places();
        public static Places Baumhoehle = new Places();
        public static Places Kuppelbaum = new Places();

        //Creation of Objects (Characters)
        public static Characters Protagonist = new Characters("Lago", false, "Male", "Red", Fuchsbau);
        public static Characters Lyncas = new Characters("Lyncas", true, "Male", "Rötlich", Weg_Versamlungsplatz);
        public static Characters Aarietes = new Characters("Aarietes", true, "Male", "Red", Versammlungsplatz);
        public static Characters Arctanis = new Characters("Arctanis", true, "Male", "Grau", Versammlungsplatz);
        public static Characters Varianus = new Characters("Varianus", true, "Male", "Weiß", Zuhause_der_Eule);
        public static Characters der_Erschaffer = new Characters("Der Erschaffer", true, "Neutral", "Braun", Kuppelbaum);
        public static Characters Hase = new Characters("Hase", true, "Male", "Braun", Versammlungsplatz);

        //Creation of Objects (Quests)
        public static Quests Besuche_das_SummendeTal = new Quests();
        public static Quests Suche_die_Eule = new Quests();

        public static void Main(string[] args)
        {
            //Background Music:
            if (ConfigurationManager.AppSettings.Get("Music") == "true")
            {
                SoundPlayer player = new SoundPlayer();
                player.SoundLocation = AppDomain.CurrentDomain.BaseDirectory + "Language\\backgroundmusic.wav";
                player.PlayLooping();
                //player.Play();
            }
            //Things that need to be done before the Game Starts:
            Console.WriteLine("Loading please Wait");
            Text.Import();

            //Filling of Objects (Places)
            //The Use of a Constructor for The Places, Quests and Species don't work because the Fill Method needs to run after the Text.Import() Method to fill the Description of the Places.
            Fuchsbau.Fill("Fuchsbau");
            Weg_Versamlungsplatz.Fill("Weg Versamlungsplatz");
            Weg_großes_Tor.Fill("Weg großes Tor");
            Versammlungsplatz.Fill("Versammlungsplatz");
            Großes_Tor.Fill("Großes Tor");
            Schneise_durch_Wald.Fill("Schneise durch Wald");
            Fluss_Ufer_Ost.Fill("Fluss Ufer Ost");
            Weg_durch_Wald.Fill("Weg durch Wald");
            Fluss_Ufer_West.Fill("Fluss Ufer West");
            Eulen_Hügel.Fill("Eulen Hügel");
            Zuhause_der_Eule.Fill("Zuhause der Eule");
            Fluss_Ufer_Süden.Fill("Fluss Ufer Süden");
            Links_Wald.Fill("Links Wald");
            Mitte_Wald.Fill("Mitte Wald");
            Rechts_Wald.Fill("Rechts Wald");
            Kristall_Höhle.Fill("Kristall Höhle");
            Schlucht.Fill("Schlucht");
            Kristall_Wald.Fill("Kristall Wald");
            Tal.Fill("Tal");
            Baumhöhle.Fill("Baumhöhle");
            Baumhoehle.Fill("Baumhoehle");
            Kuppelbaum.Fill("Kuppelbaum");
            //Filling of Objects (Characters) (Fill is from the Class Characters and FillSpezie is inherited from the Class Animalspezies
            Protagonist.FillSpezie("Fuchs", "Klein", "Weiß", "Fell");
            Lyncas.FillSpezie("Luchs", "Klein", "Rötlich", "Fell");
            Aarietes.FillSpezie("Hirsch", "Groß", "Red", "Fell");
            Arctanis.FillSpezie("Wolf", "Mittel", "Grau", "Fell");
            Varianus.FillSpezie("Eule", "Klein", "Weiß", "Federn");
            der_Erschaffer.FillSpezie("Baum", "Riesig", "Braun", "Rinde");
            Hase.FillSpezie("Hase", "Klein", "Braun", "Fell");

            //Filling of Objects (Places) things like next place and previous place needs to be added
            Fuchsbau.NextPlaces.Add(Weg_Versamlungsplatz);
            Fuchsbau.People.Add(Protagonist);

            Weg_Versamlungsplatz.NextPlaces.Add(Versammlungsplatz);
            Weg_Versamlungsplatz.PreviousPlace.Add(Fuchsbau);
            Weg_Versamlungsplatz.People.Add(Lyncas);

            Versammlungsplatz.NextPlaces.Add(Weg_großes_Tor);
            Versammlungsplatz.PreviousPlace.Add(Weg_Versamlungsplatz);
            Versammlungsplatz.People.Add(Lyncas);
            Versammlungsplatz.People.Add(Aarietes);
            Versammlungsplatz.People.Add(Arctanis);
            Versammlungsplatz.People.Add(Hase);

            Weg_großes_Tor.NextPlaces.Add(Großes_Tor);
            Weg_großes_Tor.PreviousPlace.Add(Versammlungsplatz);
            Weg_großes_Tor.People.Add(Lyncas);

            Großes_Tor.NextPlaces.Add(Schneise_durch_Wald);
            Großes_Tor.NextPlaces.Add(Weg_durch_Wald);
            Großes_Tor.PreviousPlace.Add(Weg_Versamlungsplatz);
            Großes_Tor.PreviousPlace.Add(Fluss_Ufer_Ost);

            Schneise_durch_Wald.NextPlaces.Add(Fluss_Ufer_Ost);
            Schneise_durch_Wald.PreviousPlace.Add(Großes_Tor);

            Fluss_Ufer_Ost.NextPlaces.Add(Großes_Tor);
            Fluss_Ufer_Ost.PreviousPlace.Add(Schneise_durch_Wald);

            Weg_durch_Wald.NextPlaces.Add(Fluss_Ufer_West);
            Weg_durch_Wald.PreviousPlace.Add(Großes_Tor);

            Fluss_Ufer_West.NextPlaces.Add(Eulen_Hügel);
            Fluss_Ufer_West.PreviousPlace.Add(Weg_durch_Wald);

            Eulen_Hügel.NextPlaces.Add(Zuhause_der_Eule);
            Eulen_Hügel.PreviousPlace.Add(Fluss_Ufer_West);

            Zuhause_der_Eule.NextPlaces.Add(Fluss_Ufer_Süden);
            Zuhause_der_Eule.PreviousPlace.Add(Eulen_Hügel);
            Zuhause_der_Eule.People.Add(Varianus);

            Fluss_Ufer_Süden.NextPlaces.Add(Links_Wald);
            Fluss_Ufer_Süden.NextPlaces.Add(Mitte_Wald);
            Fluss_Ufer_Süden.NextPlaces.Add(Rechts_Wald);
            Fluss_Ufer_Süden.PreviousPlace.Add(Zuhause_der_Eule);
            Fluss_Ufer_Süden.PreviousPlace.Add(Links_Wald);
            Fluss_Ufer_Süden.PreviousPlace.Add(Rechts_Wald);
            Fluss_Ufer_Süden.People.Add(Varianus);
            Fluss_Ufer_Süden.ClearPreviousText = true;

            Links_Wald.NextPlaces.Add(Fluss_Ufer_Süden);
            Rechts_Wald.NextPlaces.Add(Fluss_Ufer_Süden);

            Mitte_Wald.NextPlaces.Add(Kristall_Wald);
            Mitte_Wald.NextPlaces.Add(Schlucht);
            Mitte_Wald.NextPlaces.Add(Kristall_Höhle);
            Mitte_Wald.PreviousPlace.Add(Fluss_Ufer_Süden);
            
            Schlucht.NextPlaces.Add(Mitte_Wald);
            Schlucht.PreviousPlace.Add(Mitte_Wald);

            Kristall_Wald.NextPlaces.Add(Tal);
            Kristall_Wald.PreviousPlace.Add(Mitte_Wald);

            Tal.NextPlaces.Add(Baumhoehle);
            Tal.PreviousPlace.Add(Kristall_Wald);

            Kristall_Höhle.NextPlaces.Add(Baumhöhle);
            Kristall_Höhle.PreviousPlace.Add(Mitte_Wald);

            Baumhöhle.NextPlaces.Add(Kuppelbaum);
            Baumhöhle.PreviousPlace.Add(Kristall_Höhle);

            Baumhoehle.NextPlaces.Add(Kuppelbaum);
            Baumhoehle.PreviousPlace.Add(Tal);

            Kuppelbaum.PreviousPlace.Add(Baumhöhle);
            Kuppelbaum.PreviousPlace.Add(Baumhoehle);
            Kuppelbaum.People.Add(Hase);
            Kuppelbaum.IstEndRoom = true;

            //Filling of Objects (Quests)
            Suche_die_Eule.Fill("Suche die Eule", Aarietes, Varianus);
            Besuche_das_SummendeTal.Fill("Besuche das SummendeTal", Varianus, der_Erschaffer);
            
            Aarietes.QuestGiver.Add(Suche_die_Eule);
            Varianus.QuestGiver.Add(Besuche_das_SummendeTal);
            Varianus.QuestReceiver.Add(Suche_die_Eule);
            der_Erschaffer.QuestReceiver.Add(Besuche_das_SummendeTal);
            //Game, This runs when the Game Starts
            Console.Clear();
            Text.Write(Text.Output.Get("Other_Beginning", 0));
            Text.WriteNoNewLine("[Enter]");
            string PlaceholderForNotExitingTheAPP = Console.ReadLine();
            Console.Clear();
            Text.Write(Text.Output.Get("Other_Beginning", 1));
            Fuchsbau.Enter();
        }
    }
}
