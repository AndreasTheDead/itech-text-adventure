# ITech - Text Adventure
This Console Game was Created as a School-Project for the ITech in Hamburg.
The People who create it were Dustin, Jona and Andreas.

[[_TOC_]]


## Playing
We recommend that the Text size of the Console-Window is set to 28 wile playing the game.


## Object Diagramm
The object Diagram is to be found in [SonstigeAbgaben](https://gitlab.com/AndreasTheDead/itech-text-adventure/-/tree/master/SonstigeAbgaben)
There is a .xml file which can be opened with [diagrams.net](https://app.diagrams.net/).
Alternative there a png files which contains in the Diagrams.  


## Localisation
A Translation of the Game is Possible. For this there is a Folder "Language" in the Install Folder. There are subfolders with each Language. The Subfolders need to have the Same Name and Files but you can Translate the Content of the Files. The Gane will load the translatet Files at each Start dynamicly.
The Language can be set in the Install folder in the File "TextAdventure.exe.config".

To get a String from the Language files the Program runs Text.Output.Get("Folder_File",Index).


## Background Music
There is Background Music. You can disable the Background Music in the "TextAdventure.exe.config" file. There you will need to set "music" to "false".


## Character Creation
To create a Character some things need to be Added.<br>

### 1.	Create object in TextAdventure.Program
```c#
public static Characters Protagonist = new Characters();
```
### 2.	Fill The Object with data in TextAdventure.Program.Main
.Fill(string Name, Bool IsNPC, string Gender, string Haircolor, Places CurrentPlace)<br>
.FillSpezie(string Name, string Size, string Color, string Skin)<br>
```c#
Protagonist.Fill("MainPlayer", false, "Male", "Red", Fuchsbau);
Protagonist.FillSpezie("Fuchs", "Klein", "Weiß", "Fell");
```

### 3. Usage of TalkTo (Reden mit)
If you want to use TalkTo You will need Also to create Some Files and Folders:<br>
Language -> [DE] -> Characters -> [Charactername]_[Place].json<br>
Language -> [DE] -> Characters -> [Charactername]_[Place]_[awnser].json

The Files need to look like this:<br>
[Charactername]_[Place].json:<br>
```json
{
  "0":"Sentence one",
  "1":"Sentence two",
  "2":"Reaction to option One;Reaction to option Two;Reaction to option Three",
  "3":"Sentence Four"
  "4":"You talked already with me"
}
```
[Charactername]_[Place]_[answers].json:<br>
```json
{
  "0":"Answer One",
  "1":"Answer option One;Answer option Two;Answer option Three",
  "2":"Answer Three",
  "3":"Answer Four"
}
```


## Places Creation
### 1.	Create object in TextAdventure.Program
```c#
public static Places Fuchsbau = new Places("Fuchsbau");
```
### 2.	Set the Previis and Next Places in TextAdventure.Program.Main
.NextPlaces.Add(Places NextPlace)<br>
.PreviousPlace.Add(Places PreviusPlace)<br>
```c#
Fuchsbau.NextPlaces.Add(NextPlace);
Fuchsbau.PreviousPlace.Add(NextPlace);
```
### 3. File Creation
Each Place needs to have a File where the Description is written Down:<br>
Language -> [DE] -> Places -> [Place].json<br>
Filled with:<br>
```json
{
  "0": "Description",
  "1": "Text output leaving"
}
```
### 4. Usage of Talk
If you want to use the Talk function you will need to create a second file:
Language -> [DE] -> Characters -> [Place].json<br>
Filled with:<br>
```json
{
  "0": "First Sentence",
  "1": "Second Sentence",
  "2": "Third Sentence"
}
```


## Quest Creation
### 1.	Create object in TextAdventure.Program
```c#
public static Quests QuestName = new Quests();
```
### 2.	Fill The Object with data in TextAdventure.Program.Main
.Fill(string QuestName, Characters employer, Characters costumer)
```c#
QuestName.Fill(QuestName, employer, costumer);
```
###	3. Add Quest to The Employer and Costumer
.QuestGiver.Add(Quests QuestName)<br>
.QuestReceiver.Add(Quests QuestName)<br>
```c#
Lyncas.QuestGiver.Add(QuestName)
Lyncas.QuestReceiver.Add(QuestName)
```

### 4. Create a Language File
Each Quest needs a Language File with the Description<br>
Language -> [DE] -> Quests -> [QuestName].json<br>
Filled with:<br>
```json
{
  "0": "Description"
}
```

## Installer
The Installer is Created with [InnoSetup](https://jrsoftware.org/isinfo.php). This Software makes it Easy to create a one File Installer.
The Files for this is be Found in the InnoSetup Folder